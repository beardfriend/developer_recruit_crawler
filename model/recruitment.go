package model

type Recruitment struct {
	Title       string
	Url         string
	ImageUrl    string
	CompanyName string
	Location    string
}
